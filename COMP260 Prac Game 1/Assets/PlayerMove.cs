﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

	public float maxSpeed = 5.0f; // in metres per second
	public float acceleration = 3.5f; // in metres/second/second
	public float brake = 5.0f; // in metres/second/second
	private float speed = 0.0f;    // in metres/second
	public float turnSpeed = 40.0f; // in degrees/second
	public int playerID;
	private string hAxis;
	private string vAxis;
	private string fire;
	private BeeSpawner beeSpawner;
	public float destroyRadius = 1.5f;
	//public GameObject bangSprite;
	//private GameObject bang;

	void Start() {
		beeSpawner = FindObjectOfType<BeeSpawner> ();
		if (playerID == 0) {
			hAxis = "Horizontal";
			vAxis = "Vertical";
			fire = "Fire1";
		} else {
			hAxis = "Horizontal2";
			vAxis = "Vertical2";
			fire = "Fire2";
		}
	}

	void Update() {
		
		if (Input.GetButtonDown (fire)) {
			//bang = Instantiate(bangSprite);
			//bang.transform.position = transform.position;
			// destroy nearby bees
			beeSpawner.DestroyBees(
				transform.position, destroyRadius);
		}

		// the horizontal axis controls the turn
		float turn = Input.GetAxis(hAxis);

		// turn the car
		transform.Rotate(0, 0, turn * -turnSpeed * Time.deltaTime * speed);

		// the vertical axis controls acceleration fwd/back
		float forwards = Input.GetAxis(vAxis);
		if (forwards > 0) {
			// accelerate forwards
			speed = speed + acceleration * Time.deltaTime;
		} 
		else if (forwards < 0) {
			// accelerate backwards
			speed = speed - acceleration * Time.deltaTime;
		} 
		else {
			// braking
			if (speed < 0.01 | speed > -0.01) {
				speed = 0;
			} else if (speed > 0) {
				speed = speed - brake * Time.deltaTime;
			} else if (speed < 0) {
				speed = speed + brake * Time.deltaTime;
			}
		}

		// clamp the speed
		speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);

		// compute a vector in the up direction of length speed
		Vector2 velocity = Vector2.up * speed;

		// move the object
		transform.Translate(velocity * Time.deltaTime, Space.Self);

		//Destroy(bang.gameObject, bang.GetComponent<Animation>().duration);
	}


}
