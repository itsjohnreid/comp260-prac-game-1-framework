﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {

	public float minSpeed, maxSpeed;
	public float minTurnSpeed, maxTurnSpeed;

	private float speed = 4.0f;        // metres per second
	private float turnSpeed = 180.0f;  // degrees per second
	private Transform trans1;
	private Transform trans2;
	private Vector2 heading = Vector3.right;
	private Vector2 direction1;
	private Vector2 direction2;
	Vector2 target;

	public ParticleSystem explosionPrefab;

	void Start() {
		PlayerMove[] players = FindObjectsOfType (typeof(PlayerMove)) as PlayerMove[];
		PlayerMove player1 = players[0];
		PlayerMove player2 = players[1];
		trans1 = player1.transform;
		trans2 = player2.transform;

		// bee initially moves in random direction
		heading = Vector2.right;
		float angle = Random.value * 360;
		heading = heading.Rotate(angle);

		// set speed and turnSpeed randomly 
		speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
		turnSpeed = Mathf.Lerp(minTurnSpeed, maxTurnSpeed, 
			Random.value);
	}

	void Update() {
		
		// get the vector from the bee to the target 
		direction1 = trans1.position - transform.position;
		direction2 = trans2.position - transform.position;

		// check which player is closer
		if (direction1.magnitude < direction2.magnitude) {
			target = direction1;
		}
		else { target = direction2;
		}
			
		// calculate how much to turn per frame
		float angle = turnSpeed * Time.deltaTime;

		// turn left or right
		if (target.IsOnLeft(heading)) {
			// target on left, rotate anticlockwise
			heading = heading.Rotate(angle);
		}
		else {
			// target on right, rotate clockwise
			heading = heading.Rotate(-angle);
		}
		transform.Translate(heading * speed * Time.deltaTime);
	}

	void OnDestroy() {
		// create an explosion at the bee's current position
		ParticleSystem explosion = Instantiate(explosionPrefab);
		explosion.transform.position = transform.position;
		// destroy the particle system when it is over
		Destroy(explosion.gameObject, explosion.duration);
	}

	void OnDrawGizmos() {
		// draw heading vector in yellow
		Gizmos.color = Color.yellow;
		Gizmos.DrawRay(transform.position, heading);

		// draw p1 vector in red
		Gizmos.color = Color.red;
		Gizmos.DrawRay(transform.position, target);

		// draw p2 vector in blue
		//Gizmos.color = Color.blue;
		//Gizmos.DrawRay(transform.position, direction2);
	}



}
