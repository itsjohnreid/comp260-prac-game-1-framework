﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpawner : MonoBehaviour {

	public int nBees = 30;
	public BeeMove beePrefab;
	public float xMin, yMin;
	public float width, height;
	public float minBeePeriod = 1.0f;
	public float maxBeePeriod = 5.0f;

	private float countdown = 0.0f;

	void Start() {
		// instantiate a bee
		for (int i = 0; i < nBees; i++) {
			BeeMove bee = Instantiate (beePrefab);

			bee.transform.parent = transform;

			bee.gameObject.name = "Bee " + i;

			float x = xMin + Random.value * width;
			float y = yMin + Random.value * height;
			bee.transform.position = new Vector2 (x, y);
		}
	}
	
	// Update is called once per frame
	void Update () {
		countdown -= Time.deltaTime;

		if (countdown <= 0.0f) {

			BeeMove bee = Instantiate (beePrefab);

			bee.transform.parent = transform;

			bee.gameObject.name = "Bee " + nBees;
			nBees++;

			float x = xMin + Random.value * width;
			float y = yMin + Random.value * height;
			bee.transform.position = new Vector2 (x, y);

			countdown = Random.Range (minBeePeriod, maxBeePeriod);
		}
		
	}

	public void DestroyBees(Vector2 centre, float radius) {
		// destroy all bees within ‘radius’ of ‘centre’
		for (int i = 0; i < transform.childCount; i++) {
			Transform child = transform.GetChild(i);
			// Fixed bug by adding a type conversion
			Vector2 v = (Vector2)child.position - centre;
			if (v.magnitude <= radius) {
				Destroy(child.gameObject);
			}
		}
	}
}
